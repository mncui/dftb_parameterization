
import copy
import json
import math
import os
import re

import ase
import numpy as np
from ase import Atoms

np.random.seed(66)
import random
from pathlib import Path
from time import perf_counter

import matplotlib.pyplot as plt
from ase.data import atomic_numbers, chemical_symbols, covalent_radii
from ase.units import *
from scipy.interpolate import UnivariateSpline
from scipy.optimize import minimize
from skopt import gp_minimize, load
from skopt.callbacks import CheckpointSaver
from skopt.plots import (plot_convergence, plot_evaluations,
                         plot_gaussian_process, plot_objective,
                         plot_objective_2D)
from skopt.space import Categorical, Integer, Real
from scipy.optimize import Bounds, basinhopping, brute, fmin


import multiprocessing as mp
from physrep import physrep
from hotpy import hotpy
from math import floor
import argparse
from argparse import RawTextHelpFormatter
import sys



def merge_atoms(atoms_a:Atoms, atoms_b:Atoms, prefixs=['band_', 'rep_']):
    """
    Merge two atoms object
    """
    prefix_a, prefix_b = prefixs
    atoms_total = []
    for num, atoms in enumerate(atoms_a):
        atoms_new = ase.Atoms(atoms.get_chemical_formula(), atoms.get_positions())
        atoms_new.set_cell(atoms.get_cell())
        atoms_new.set_pbc(atoms.get_pbc())
        
        atoms_new.info['energy'] = atoms.info[prefix_a + 'energy'] + atoms_b[num].info[prefix_b + 'energy']
        atoms_new.info[prefix_a + 'energy'] = atoms.info[prefix_a + 'energy']
        atoms_new.info[prefix_b + 'energy'] = atoms_b[num].info[prefix_b + 'energy']

        atoms_new.set_array('forces', atoms.get_array(prefix_a + 'forces') + atoms_b[num].get_array(prefix_b + 'forces'))
        atoms_new.set_array(prefix_a + 'forces', atoms.get_array(prefix_a + 'forces'))
        atoms_new.set_array(prefix_b + 'forces', atoms_b[num].get_array(prefix_b + 'forces'))

        atoms_new.info['structure_name'] = atoms.info['structure_name']

        atoms_total.append(atoms_new)
    return atoms_total


 
      
def get_full_dftb(dftb_bandstructure:Atoms, dft:Atoms, pr:physrep, calc_forces:bool=False):
    """
    Get the full dftb with Band Structure energy (&SCC) and Repulsive potential
    """
    atoms_total = []
    print("Calculate the repulsive energy!")
    for num, atoms in enumerate(dft):
        ref_e, ref_f = pr.evaluate_pbc(atoms)
        atoms_new = ase.Atoms(atoms.get_chemical_formula(), 
                              atoms.get_positions())
        atoms_new.set_cell(atoms.get_cell())
        atoms_new.set_pbc(atoms.get_pbc())
        
        atoms_new.info['energy'] = ref_e + dftb_bandstructure[num].info['energy']
        atoms_new.info['band_energy'] = dftb_bandstructure[num].info['energy']
        atoms_new.info['rep_energy'] = ref_e

        if calc_forces:
            atoms_new.set_array('forces', ref_f + dftb_bandstructure[num].get_array('forces'))
            atoms_new.set_array('band_forces', dftb_bandstructure[num].get_array('forces'))
            atoms_new.set_array('rep_forces', ref_f)

        try:
            atoms_new.info['structure_name'] = atoms.info['structure_name']
        except:
            pass
        atoms_total.append(atoms_new)
    return atoms_total


def get_energy_per_atom(dft:Atoms,dftb:Atoms, bolt_fxx:list=None):
    """
    Calculate the energy per atom

    Note: bolt_fxx is the Boltzmann factor for each structure, it's length is the same as dftb
    provide it while you want to calculate specific structures
    None while you want to calculate all structures, then it will automatically find from files "search_index.txt"

    """
    dftb_energy_per_atom = [dftb_mole.info['energy']/ dftb_mole.get_global_number_of_atoms() for dftb_mole in dftb]
    dft_energy_per_atom  = [dft_mole.get_total_energy()/ dft_mole.get_global_number_of_atoms() for dft_mole in dft]
    min_num = np.argmin(dft_energy_per_atom); print('min_num:', min_num)

    dftb_energy_per_atom_relative = [dftb_energy_per_atom[i] - dftb_energy_per_atom[min_num] for i in range(len(dftb_energy_per_atom))]
    dft_energy_per_atom_relative  = [dft_energy_per_atom[i]  - dft_energy_per_atom[min_num] for i in range(len(dft_energy_per_atom))]

    # delta_energy_per_atom = np.mean(np.abs(np.array(dftb_energy_per_atom_relative) - np.array(dft_energy_per_atom_relative)) )
    if bolt_fxx == None:
        with open ("search_index.txt", 'r') as search_index_file:
            search_index = search_index_file.read().strip().split(',')
        bolt_fxx = [bolt_f11[int(i)] for i in search_index]

    delta_energy_per_atom = np.sum(np.abs([dftb_energy_per_atom_relative[m] - dft_energy_per_atom_relative[m] for m in range(len(bolt_fxx)) if bolt_fxx[m] >= 0.1]) / np.sum([bolt_fxx[m] for m in range(len(bolt_fxx)) if bolt_fxx[m] >= 0.1]))

    # return mu_E
    return dftb_energy_per_atom_relative, dft_energy_per_atom_relative, delta_energy_per_atom


# def callback(par, r0, r0_dens):
#     # Callback function used to output the progress of the optimizer
#     lk = get_repulsion(par, r0, r0_dens)
#     print(par,lk)


def monkhorstpack2kptdensity(atoms, k_density):

    """Convert Monkhorst-Pack grid to k-point density.
    atoms (ase.atoms.Atoms): Atoms object.
    k_grid (list): [nx, ny, nz].
    Returns:
        float: Smallest line-density.
    """

    # assert len(k_grid) == 3, "Size of k_grid is not 3."

    recipcell = atoms.cell.reciprocal()
    kd = [] # k-point density
    ks = [] # k-point space
    # initial a array for k_grids, with 3 columns
    # k_grids = np.empty((1,3))
    k_grid_abc = []
    for i in range(3):
        if atoms.pbc[i]:
            k_grida = int(k_density * (2 * pi * sqrt((recipcell[i] ** 2).sum())))
            if k_grida == 0:
                k_grida = 1
            k_grid_abc.append(k_grida) # current k_grid
            # k_grids = np.vstack((k_grids, k_grid_abc))
            # kptdensity = k_grid[i] / (2 * pi * sqrt((recipcell[i] ** 2).sum()))
            kptspace =  sqrt((recipcell[i] ** 2).sum()) / k_grida
            ks.append(kptspace)
    return ks, k_grid_abc


def get_elements_from_dataset(dataset, opt_elem):
    
    tot_atomic_numbers = np.unique(np.array([np.unique(mole.numbers).tolist() for mole in dataset]).flatten()) 
    atomic_numbers_exclusive = [i for i in tot_atomic_numbers if i not in opt_elem]
    atomic_numbers = atomic_numbers_exclusive + opt_elem

    atomic_symbols_exclusive = [chemical_symbols[i] for i in atomic_numbers_exclusive]
    atomic_symbols = atomic_symbols_exclusive + [chemical_symbols[num] for num in opt_elem]

    return atomic_numbers, atomic_numbers_exclusive, atomic_symbols, atomic_symbols_exclusive


def set_parameters(parameters:list, 
                   sigma:float):
    """
    Set parameters as the input for Hotcent and Physrep
    """

    kxc = 0.0
    r0, r0_dens = parameters

    
    if opt_elem == None or len(opt_elem) == 0:
        #! For Single element system
        confinement_r0 = {s: [r0, r0_dens] for s in atomic_symbols}
        confinement_sigma = {s: sigma for s in atomic_symbols}
        # para_rep = [hp.atoms_info[symbol]['c_rep'] for symbol in atomic_symbols]
        # para_rep_exclusive = [hp.atoms_info[symbol]['c_rep'] for symbol in atomic_symbols if symbol not in opt_elem]


    elif len(opt_elem) > 0:
        #! For Binary system
        # For multi-element system, we need to define the cons and sigs and c_rep for each element
        confinement_r0 = {s: [hp.atoms_info[s]['r_wave'], hp.atoms_info[s]['r_dens']] for s in atomic_symbols_exclusive}
        confinement_r0[chemical_symbols[opt_elem[0]]] = [r0, r0_dens]
        # confinement_sigma = {s: sigma for s in atomic_symbols_exclusive}
        confinement_sigma = {s: sigma for s in atomic_symbols}
    
    return confinement_r0, confinement_sigma



def run_calculator_seperately(dft_incopy:Atoms, kpts_dens):

    dft_subincopy_final = []
    dftb_bandstructure_final = []
    for n in np.arange(len(dft_center)):
        dft_sub = dft_incopy[n*DEFAULT_EOS_POINTS:(n+1)*DEFAULT_EOS_POINTS]
        bolt_subf = bolt_f11[n*DEFAULT_EOS_POINTS:(n+1)*DEFAULT_EOS_POINTS]
        try:
            dftb_bandstructure = hp.run_calculator(dft_sub, 
                                                poly_rep='Yes', 
                                                SCC='Yes', 
                                                kpts_dens=kpts_dens, 
                                                bolt_f=bolt_subf)
            # poly_rep = 'Yes': use polynomial repulsion (default 0.0 here)
            # SCC = 'Yes': use self charge consistent calculation
            dft_subincopy_final.extend(dft_sub)
            dftb_bandstructure_final.extend(dftb_bandstructure)
        except FileNotFoundError as err:
            print("DFTB+ Warning:", err)
            print("The problem caused by failed generation of SKF with this parameter. We will skip the structure in this round of parameterization.")

    ase.io.write('dft_final.xyz', dft_subincopy_final)
    ase.io.write('dftb_band.xyz', dftb_bandstructure_final)
    
    return dftb_bandstructure_final, dft_subincopy_final


def run_calculator_seperately_parallel(dft_incopy:Atoms, kpts_dens, calculator_code:str, tag:str):
    # def prep_gnuparallel(dataset_infile:str, num_parall, calculator_code:str, tag:str):
    """
    Be parallelized respect to DEFAULT_EOS_POINTS

    Parameters:
    ----------
    dataset_file: str
        path to the validation dataset file
    num_parall: int
        number of parallel processes
    tag: str
        tag for the output folders
    """

    # 1. Divide input file into num_parall folders
    # initialize information
    dataset = dft_incopy#ase.io.read(dataset_infile, ':')
    dataset_file = "ref.xyz"
    # dataset_file = dataset_infile.strip().split('/')[-1]
    # dataset_file_output = dataset_file.replace('.xyz', f'_{tag}.xyz')
    dataset_file_output = "dftb_band.xyz"
    num_atoms = len(dft_incopy)
    num_parall = int(num_atoms / DEFAULT_EOS_POINTS)
    if num_parall == 0:
        num_parall = 1
    

    # seperate val_dataset into num_parall parts
    size_subs = math.ceil(num_atoms / num_parall)

    subdataset_names = [f"{tag}_{i}/tmp.{dataset_file.split('.')[0]}.xyz" for i in range(num_parall)]
    subdataset_gap_names = [f"{tag}_{i}/tmp.{dataset_file.split('.')[0]}_dftb.xyz" for i in range(num_parall)]
    print(f"subdataset_names: {'  '.join(subdataset_names)}")
    print(f"subdataset_gap_names: {'  '.join(subdataset_gap_names)}")


    # write subdataset
    cmd_file = open("cmd.lst", 'w')
    for n in range(num_parall):
        if bolt_f[n] >= 0.1:
            k_dens = kpts_dens
        else:
            k_dens = 1
        start_index = n * size_subs
        end_index = (n + 1) * size_subs
        subdataset = dataset[start_index: end_index]

        subfolder_name = f"{tag}_{n}"
        if not os.path.exists(subfolder_name):
            os.system(f"mkdir {subfolder_name}")
            os.system(f"cp {p}/{calculator_code} {subfolder_name}")
            if tag.lower() != "xtb":
                os.system(f"cp *.skf {subfolder_name}")
        else:
            os.system(f"cp {p}/{calculator_code} {subfolder_name}")
            if tag.lower() != "xtb":
                os.system(f"cp *.skf {subfolder_name}")
        subdataset_name = subdataset_names[n]  
        ase.io.write(subdataset_name, subdataset)


        # write cmd file to cmd.lst, here i run calculator with python
        cmd_run = f"python  {calculator_code} -sp tmp.{dataset_file.split('.')[0]}.xyz {tag} -d {k_dens}"

        cmd_file.write(f"cd {subfolder_name} ; {cmd_run} >> output\n")
    cmd_file.close()

    print("Seperate validation dataset into " + str(num_parall) + " parts for parallel calculation.")


    # 2. Run GAP on each part
    os.system(f"parallel --delay 0.2 --joblog task.log --progress --resume -j {num_parall} < cmd.lst")


    # 3. Collect data after finished
    os.system(f"cat {' '.join(subdataset_gap_names)} > {dataset_file_output}")
    os.system("mv task.log task_backup.log")

    output_dataset = ase.io.read(dataset_file_output, ':')
    dft_final, search_index = find_ref_by_structure(output_dataset, dft_incopy, "_", "_")
    
    # bolt_f11_final = [bolt_f11[i] for i in search_index]
    # write the search index into a file
    with open("search_index.txt", 'w') as search_index_file:
        search_index_file.write(','.join([str(i) for i in search_index]))

    ase.io.write("dft_final.xyz", dft_final)


    if len(output_dataset) == len(dataset):
        os.system(f"rm -r {tag}_*")
    else:
        print(f"WARNING: len(ref_dataset):{len(dataset)} != len(gap_dataset):{len(output_dataset)}")
    
    return output_dataset, dft_final


def find_ref_by_structure(atoms_input1, atoms_input2, in_prefix, out_prefix):

    if in_prefix == '_':
        in_prefix = ''
    if out_prefix == '_':
        out_prefix = ''

    atoms_output = []
    search_index = []
    for i, atoms1 in enumerate(atoms_input1):  # this is the reference dataset

        read_energy = f"{in_prefix}energy"
        read_forces = f"{in_prefix}forces"
        write_energy = f"{out_prefix}energy"
        write_forces = f"{out_prefix}forces"

        for j, atoms2 in enumerate(atoms_input2):

            if np.array_equal(atoms2.get_positions(),atoms1.get_positions()) and np.array_equal(atoms2.get_cell(), atoms1.get_cell()) and np.array_equal(atoms2.get_atomic_numbers(), atoms1.get_atomic_numbers()):
                print(f"Found a match from no.{j} of Dataset2 to no.{i} of Dataset1")
                atoms_output.append(atoms2)
                search_index.append(j)
                break
    return atoms_output, search_index


def get_input(dft_infile):

    dft_input = ase.io.read(dft_infile, ':')
    dft_center = dft_input[floor(DEFAULT_EOS_POINTS/2)::DEFAULT_EOS_POINTS]


    atomic_numbers, atomic_numbers_exclusive , atomic_symbols, atomic_numbers_exclusive = get_elements_from_dataset(dft_input, opt_elem)
    if len(opt_elem) == 0: # Elementary system
        bolt_f, bolt_f11 = hp.get_bolt_factor(dft_center, 'boltzmann', 3000)
        cov_radius = covalent_radii[atomic_numbers[0]] / Bohr


    else:                  # Binary system
        bolt_f, bolt_f11 = hp.get_bolt_factor(dft_center, 'identical', 3000)
        cov_radius = covalent_radii[opt_elem[0]] / Bohr
    
    return dft_input, dft_center, atomic_numbers, atomic_numbers_exclusive, atomic_symbols, atomic_numbers_exclusive, bolt_f, bolt_f11, cov_radius


def parallel_repulsion(c_rep:list, r0:float, r0_dens:float, write_rep:bool=False):
    """
    Make the repulsive potential parallel across different structures

    Parameters
    ------
    c_rep: list
        Parameter for repulsive potential
        eg: c_rep = array([0.4321])
    
    r0: float
        Parameter for wavefunction cutoff
    
    r0_dens: float
        Parameter for density function cutoff
    
    """
    c_rep = c_rep.tolist()
    pool = mp.Pool()

    # Divide dataset into different groups
    dftb_bandstructure_final = ase.io.read('dftb_band.xyz', ':')
    dft_final = ase.io.read('dft_final.xyz', ':')
    dft_center_final = dft_final[floor(DEFAULT_EOS_POINTS/2)::DEFAULT_EOS_POINTS]
    dft_incopy = copy.deepcopy(dft_final)
    dft_subincopy = [dft_incopy[i*DEFAULT_EOS_POINTS:(i+1)*DEFAULT_EOS_POINTS] for i in range(len(dft_center_final))]

    for num, dft_sub in enumerate(dft_subincopy):
        # print(f"dft_sub: {num}")
        dftb_bandstructure = dftb_bandstructure_final[num*DEFAULT_EOS_POINTS:(num+1)*DEFAULT_EOS_POINTS]

        atomic_subnumbers, atomic_subnumbers_exclusive , _, _ = get_elements_from_dataset(dft_sub, opt_elem)


        # I want the list in sequence, the optimized element in the end
        if len(opt_elem) == 0:
            para_rep_tot = c_rep
        else:
            para_rep_tot = [hp.atoms_info[chemical_symbols[n]]['c_rep'] for n in atomic_subnumbers_exclusive] + c_rep
        # atomic_subnumbers_tot = atomic_subnumbers.tolist() #+ opt_elem
        kxc_tot = [0.0] * len(atomic_subnumbers)

        
        # Prepare input for parallel
        input_parameters = [(num, dftb_bandstructure, dft_sub, atomic_subnumbers, para_rep_tot, kxc_tot, write_rep)]

        outputs = pool.imap(generate_repulsion, input_parameters)
    
    pool.close()
    pool.join()

    # Judge which loss function to use

    if args_input.target is not None:
        mu = collectAndcalculate_muEF(dft_incopy, c_rep, r0, r0_dens)
    elif args_input.optimization_option is not None:
        mu = collectAndcalculate_muEV(dft_incopy, c_rep, r0, r0_dens)
    else:
        raise ValueError("Please set the target (with dataset) or optimization_option")
    return mu

def collectAndcalculate_muEV(dft_incopy:Atoms, c_rep, r0, r0_dens):


    num_parall = int(len(dft_incopy) / DEFAULT_EOS_POINTS)
    dftb_full, mu_Es, mu_Vs, mu_logbs = [], [], [], []
    for num, dft_sub in enumerate(dft_subincopy):
        dftb_sub = ase.io.read(f'dftb_sub_{num}.xyz', ':')
        dftb_full.extend(dftb_sub)

        # eos_dftb = hp.eos_dftb(dftb_sub, dft_center)
        # eos_dft  = hp.eos_aims(dft_sub, dft_center)
        eos_dftb = hp.eos_atoms(dftb_sub, DEFAULT_EOS_POINTS, 'dftb')
        eos_dft  = hp.eos_atoms(dft_sub, DEFAULT_EOS_POINTS, 'dft')
        print(f"\neos_dftb: {eos_dftb}\neos_dft: {eos_dft}")


        # Calculate the mu_V
        [eos_dft[conf].update(eos_dftb[conf]) for conf in eos_dftb] # input eos of dft
        mu_cal, mu_V, mu_logb = hp.eos_cal(eos_dft, [bolt_f[num]]) 
        mu_Vs.append(mu_V); mu_logbs.append(mu_logb)
        
        mu_Es.append(dftb_sub[0].info['mu_E'] * bolt_f[num]) 

    bolt_left = np.sum([bolt_f[i] for i in range(len(bolt_f)) if bolt_f[i] >= 0.1])
    mu_E, mu_V, mu_logb = np.sum(mu_Es)/bolt_left, np.sum(mu_Vs)/bolt_left, np.sum(mu_logb)/bolt_left

    ase.io.write('dftb_full.xyz', dftb_full)
    os.system("rm dftb_sub_*.xyz")

    # Penalty term(regularization) to avoid overfitting parameters
    penalty = ((r0 / (2*cov_radius) - 1)**2 + 1)**2 * ((r0_dens / (4*cov_radius) - 1)**2 + 2)**2


    # Combine mu_E and mu_V
    c = 0.5
    mu = (c * mu_V + (1 - c) * mu_E) * penalty 


    # add just type to save information
    c_rep, kxc = c_rep[0], 0.0
    with open(output_file, 'a') as output:
        output.write(f'{r0:15.8f}, {r0_dens:15.8f}, {c_rep:15.8f}, {kxc:20.8e}, {cov_radius:15.8f}, {mu_E:15.8f}, {mu_V:20.8f}, {mu_logb: 20.8f}, {penalty:15.5f}, {mu:25.11f}\n')


    # return #dftb_full, mu_E, mu_V, mu_logb
    return mu



def generate_repulsion(input_parameters):

    n, dftb_bandstructure, dft_sub, atomic_subnumbers_tot, para_rep_tot, kxc_tot, write_rep = input_parameters
    print(f"\n> Subparallel Physrep Info: {n}\n  atomic_subnumbers_tot:{atomic_subnumbers_tot}\n  para_rep_tot:{para_rep_tot}\n  kxc_tot:{kxc_tot}\n len(dftb_band):{len(dftb_bandstructure)}\n len(dft_sub):{len(dft_sub)}\n write_rep:{write_rep}")


    pr = physrep(elements=atomic_subnumbers_tot,
                rmin=0.5,
                rmax=20.0,
                npoints=666,
                scale_cov=para_rep_tot,
                k_xc=kxc_tot,
                )
    
    if write_rep:
        if len(opt_elem) > 0:
            pr.write_specific_skf(opt_elem)
            print("Write specific SKF for Binarys")
        else:
            pr.write_skf()
            print("Write SKF for Elementary")

    
    atoms_total = get_full_dftb(dftb_bandstructure, dft_sub, pr, False)

    with open ("search_index.txt", 'r') as search_index_file:
        search_index = search_index_file.read().strip().split(',')
        bolt_f11_final = [bolt_f11[int(i)] for i in search_index]

    dftb_energy_per_atom_relative, dft_energy_per_atom_relative, delta_energy_per_atom = get_energy_per_atom(dft_sub, atoms_total, bolt_f11_final[n*DEFAULT_EOS_POINTS:(n+1)*DEFAULT_EOS_POINTS])

    for atoms in atoms_total:
        atoms.info['mu_E'] = delta_energy_per_atom
    
    ase.io.write(f'dftb_sub_{n}.xyz', atoms_total)

    return atoms_total, delta_energy_per_atom


def Repulsion_optimization(parameters:list):
    # Set search boundary, Note that please set 0.1, 1.5 for Hydrogen
    r0, r0_dens = parameters
    srange = (0.1, 0.8)
    bounds = (Bounds(srange[0], srange[1], keep_feasible=True))

    
    # # Search time 1: Gride search
    conf_args = (r0, r0_dens, False)
    res_brute = brute(parallel_repulsion, (srange,), Ns=11, full_output=True,  disp=True, workers=1, args=conf_args)
    print(f"Search time 1: c_rep = {res_brute[0]}, mu = {res_brute[1]}")

    # Search time 2: L-BFGS-B
    res_bfgs = minimize(parallel_repulsion, [res_brute[0]], method='L-BFGS-B',bounds=bounds, options={'disp':True,'maxfun':20,'ftol':1e-3}, args=conf_args)
    c_rep = res_bfgs.x[0]
    mu = res_bfgs.fun
    print(f"Search time 2: c_rep = {c_rep}, mu = {mu}")
    # c_rep, mu = 0.5528630218453833, 12.37845357698533
    return c_rep, mu


def EnergyGeometry(parameters:list,
                   xc:str,
                   sigma:float,
                   superposition:str,
                   ) -> float:
    """
    Calculate the loss function for Energy and Geometry

    Parameters:
    -------
    parameters: list
        List of parameters to be optimized
        eg: parameters = [r0, r0_dens]
    
    xc: str
        Exchange and Correlation functional
        eg: xc = 'GGA_X_PBE+GGA_C_PBE'
    
    kxc: float
        Parameter for exchange and correlation functional
        eg: kxc = 0.0
    
    sigma: float
        Parameter for confinement
        eg: sigma = 2.0
    
    superposition: str
    """

    kxc = 0.0
    r0, r0_dens = parameters

    
    opt_directory = p / DEFAULT_OUT_DIR / f"opt_{r0:.3f}_{r0_dens:.3f}"
    opt_directory.mkdir(exist_ok=True, parents = True)

    #!=================================== Change Folder ===================================!#
    os.chdir(opt_directory)


# ============================================================================ #
#   1. Generate SKF 

    t_ini = perf_counter()

    confinement_r0, confinement_sigma = set_parameters(parameters, 
                                                       sigma)

    print(f"Confinement_r0:{confinement_r0}, Confinement_sigma:{confinement_sigma}")
    hp.set_variables(confinement_r0,
                     confinement_sigma, 
                     xc=xc,
                     superposition=superposition,
                    )

    try:
        # Write  SKF file
        hp.full_hotcent(superposition=superposition,
                        opt_elem=opt_elem,
                        )        

        t_hotcent = perf_counter()

    
# ============================================================================ #
#  2. DFTB Calculation for Band Structure Energy term


        dft_incopy = copy.deepcopy(dft_input)
        kpts_dens = DEFAULT_KPOINTS_DENSITY


        # The dftb_bandstructure is refer to DFTB1 not real band structure   
        dftb_bandstructure_final, dft_subincopy_final = run_calculator_seperately_parallel(dft_incopy, kpts_dens, 'calc_dftb.py', 'static')
        # dft_subincopy_final = ase.io.read('dft_final.xyz', ':'); dftb_bandstructure_final = ase.io.read('dftb_band.xyz', ':')

        t_dftb = perf_counter()

# ============================================================================ #
#  3. Parameterization for the Repulsion term

        c_rep, mu = Repulsion_optimization(parameters)


        # Write REP into SKF file
        # Calculate repulsive potential based on the optimized parameter
        # tot_para_rep = para_rep + [c_rep]
        if len(opt_elem) == 0:
            tot_para_rep = [c_rep]
        else:
            tot_para_rep = [hp.atoms_info[chemical_symbols[n]]['c_rep'] for n in atomic_numbers_exclusive] + [c_rep]


        pr = physrep(atomic_numbers,  
                     rmin=0.5,
                     rmax=20.0,
                     npoints=666,
                     scale_cov=tot_para_rep,
                     k_xc=[0.0] * len(atomic_numbers),
                     )

        if len(opt_elem) > 0:
            pr.write_specific_skf(opt_elem)
        else:
            pr.write_skf()

        
        dft_subincopy_final = ase.io.read('dft_final.xyz', ':'); dftb_bandstructure_final = ase.io.read('dftb_band.xyz', ':')
        dftb_full = get_full_dftb(dftb_bandstructure_final, dft_subincopy_final, pr, False)
        ase.io.write('dftb_full.xyz', dftb_full)
        
        
        t_physrep = perf_counter()

# ============================================================================ #
#  4. Data processing and output

        # Calculate the energy per atom and plot
        with open ("search_index.txt", 'r') as search_index_file:
            search_index = search_index_file.read().strip().split(',')
        bolt_f11_final = [bolt_f11[int(i)] for i in search_index]

        dftb_energy_per_atom_relative, dft_energy_per_atom_relative, delta_energy_per_atom = get_energy_per_atom(dft_subincopy_final, dftb_full, bolt_f11_final)


        # Plot
        if opt_elem == None or len(opt_elem) == 0:
            hp.plot_vs(atomic_symbols[0], [r0, r0_dens, c_rep, mu], dft_incopy, dft_energy_per_atom_relative, dftb_energy_per_atom_relative, bolt_f)

            hp.plot_tradition(atomic_symbols[0], [r0, r0_dens, c_rep, 2.0],  mu, dft_energy_per_atom_relative, dftb_energy_per_atom_relative, bolt_f)
        

        penalty = ((r0 / (2*cov_radius) - 1)**2 + 1)**2 * ((r0_dens / (4*cov_radius) - 1)**2 + 2)**2

        with open(para_file, 'a') as para:
            para.write(f'{r0:15.8f}, {r0_dens:15.8f}, {c_rep:15.8f}, {kxc:20.8e}, {cov_radius:15.7f}, {mu:25.11f}\n')


        t_end = perf_counter()

        # os.chdir(p / DEFAULT_OUT_DIR)        
        print(f"""\nTime:
Hotcent: {t_hotcent - t_ini}
DFTB: {t_dftb - t_hotcent}
Physrep: {t_physrep - t_dftb}
DataProcessing: {t_end - t_physrep}
Total: {t_end - t_ini}""")              
        return mu
        

    # Except Errors caused by unphysical parameters
    except(RuntimeError, AssertionError, UnboundLocalError) as error:
        print("Error:", error)
        c_rep, kxc = [0.4321, 0.0]
        mu_E, mu_V, mu_logb, penalty = [10, 10, 10, 10]
        mu = random.randint(97,99)

        with open(para_file, 'a') as para:
            para.write(f'{r0:15.8f}, {r0_dens:15.8f}, {c_rep:15.8f}, {kxc:20.8e}, {cov_radius:15.7f}, {mu:25.11f}\n')
        
        with open(output_file, 'a') as output:
            output.write(f'{r0:15.8f}, {r0_dens:15.8f}, {c_rep:15.8f}, {kxc:20.8e}, {cov_radius:15.8f}, {mu_E:15.8f}, {mu_V:20.8f}, {mu_logb: 20.8f}, {penalty:15.5f}, {mu:25.11f}\n')
        return mu
    


def Loss_EnergyGeometry(parameters:list) -> float:
    """
    Calculate Loss function based on Energy and Geometry

    Parameters:
    -------
    parameters : list
        List of parameters to be optimized
        eg: parameters = [r0, r0_dens]
    """

    superposition = DEFAULT_SUPERPOSITION
    xc = DEFAULT_XC


    default_sigma = 2.0
    r0, r0_dens = parameters

    mu = EnergyGeometry(parameters,
                        xc=xc,
                        sigma=default_sigma,
                        superposition=superposition,
                        )

    return mu

def Bayesian_optimization(cov_radius, n_calls):
    """

    """

    if DEFAULT_CHECKPOINT_FILE.exists():
        res = load(DEFAULT_CHECKPOINT_FILE)
        x0 = res.x_iters
        y0 = res.func_vals 
        n_calls = n_calls  # How many times do you want to run still?
    else:
        x0 = DEFAULT_START_PARA
        y0 = None
        n_calls = n_calls   

    
    r0_lower, r0_upper = 1.9, 5.0 * cov_radius            # Wavefunction cutoff
    r0_dens_lower, r0_dens_upper = 3.0, 8.0 * cov_radius  # Density function cutoff

    dr0   = Real(name='r0',  low=r0_lower,  high=r0_upper,   prior='uniform')
    dr0_dens   = Real(name='r0^d',  low=r0_dens_lower,  high=r0_dens_upper,   prior='uniform')
    dimensions = [dr0, dr0_dens]

    res = gp_minimize(func=Loss_EnergyGeometry,
                    dimensions=dimensions,
                    n_calls=n_calls,
                    acq_func="EI",
                    x0=x0,
                    y0=y0,
                    callback=[checkpoint_saver],
                    random_state=1234)
    
    return res




# sys.argv = ['DftbParameterization.py', '-opt', 'energygeometry', '-input', '../dft']
parser = argparse.ArgumentParser(description='THE CODE DESIGNED FOR THE PARAMETERIZATION OF DFTB', formatter_class=RawTextHelpFormatter)

# parser.add_argument('-opt', '--optimization_target', 
#                     type=str, 
#                     default='', 
#                     help='-opt EnergyGeometry/BandStructure/Dataset')

parser.add_argument('-opt', '--optimization_option',
                    nargs='+',
                    type=str,
                    default=None,
                    help="The optimization options: \n(1) -opt band rep: optimize both confinement and repulsive potential \
                    \n(2) -opt rep: optimize only repulsive potential \
                    \n(3) -opt band: optimize only confiment potential, \
                    \n where the target are also various from energy&volume, band structure and energy&volume")

parser.add_argument('-input', '--input_dir', 
                    type=str, 
                    default='dft/', 
                    help='The folder that include reference data for the parameterization! \
                    \neg. dft.xyz or band.json for various purposes!')

parser.add_argument('-user', '--user_defined_parameters',
                    nargs='+',
                    type=float,
                    default=None,
                    help='This is for debug or use user defined parameters \neg. -user 3.456, 5.678, 0.5, 2.0!')



args_input = parser.parse_args()


# ============================================================================ #
# 1. Initialize some logfile
# ============================================================================ #
DEFAULT_OUT_FILE = 'log.out'  # Output LOG file
DEFAULT_PARA_FILE = 'par.out' # Output parameter file for each iteration
DEFAULT_OUT_DIR = 'results'   # Output directory for each iteration
DEFAULT_CHECKPOINT_FILE = Path('./checkpoint.pkl') 
DEFAULT_SUPERPOSITION = 'density'
DEFAULT_XC = 'GGA_X_PBE+GGA_C_PBE'
DEFAULT_KPOINTS_DENSITY = 4.5
DEFAULT_DFT_FILE = 'dfts.xyz'
DEFAULT_DFT_EOS_FILE = 'fit.json'
DEFAULT_EOS_POINTS = 11
# ============================================================================ #


opt_elem = []

p = Path.cwd()
hp = hotpy()
output_file = p / DEFAULT_OUT_FILE
para_file = p / DEFAULT_PARA_FILE
checkpoint_saver = CheckpointSaver(f"{p}/checkpoint.pkl", compress=9, store_objective=False)  # lifesaver


# ============================================================================ #
# 2.1 Energy and Geometry
# if args_input.optimization_target.lower() == 'energygeometry':
t0     = perf_counter()

if args_input.optimization_option is not None:

    # Get the optimization option
    opt_option = args_input.optimization_option

    if 'rep' in opt_option:
        with open(output_file, 'w') as output:
            output.write('#r0,r0_dens,c_rep,kxc,cov_radius,mu_E,mu_V,mu_B,penalty,mu\n')

        with open(para_file, 'w') as para:
            para.write('#r0,r0_dens,c_rep,kxc,cov_radius,mu\n')
        # Set optimization element
        # In the PTBP article, we design two different optimization target
        # (1) Elementary system Y
        # - where only one element is needed for parameterization, therefore opt_elem = []
        
        # (2) Binary system X-Y
        # - where X=C, B, Si, ...
        # Y means the dimer elements that we want to optimize (we definitely dont want to optimized all elements at once, cause it is too expensive), in this case opt_elem = [Y]


        dft_infile = p / args_input.input_dir / DEFAULT_DFT_FILE
        assert dft_infile.exists(), f'Seems that the input path {args_input.input_file} does not exist!'

        dft_input = ase.io.read(dft_infile, ':')
        dft_center = dft_input[floor(DEFAULT_EOS_POINTS/2)::DEFAULT_EOS_POINTS]
        num_center_atoms = len(dft_center)


        # Get the atomic number of the system
        atomic_numbers, atomic_numbers_exclusive , atomic_symbols, atomic_symbols_exclusive = get_elements_from_dataset(dft_input, opt_elem)
        cov_radius = covalent_radii[atomic_numbers[-1]] / Bohr


        if len(opt_elem) == 0: # Elementary system
            print("Set boltmann factor for Elementary System!")
            bolt_f, bolt_f11 = hp.get_bolt_factor(dft_center, 'boltzmann', 3000)
        else:                  # Binary system
            print("Set boltmann factor for Binary System!")
            bolt_f, bolt_f11 = hp.get_bolt_factor(dft_center, 'identical', 3000)
        

        # assert dft_center[0].info['structure_name'], 'Seems that the "structure name" is not set!'
        # configs = [mole.info['structure_name'] for mole in dft_center]

        print(f"""#===================================Input===================================#:
    Optimization target: {args_input.optimization_option}
    Input file: {dft_infile}
    Number of structure: {len(dft_input)}
    Number of center atoms: {num_center_atoms}
    Atomic number: {atomic_numbers}
    Covalent radius: {cov_radius}
    Boltzmann factor: {bolt_f.tolist()}
    """)
        

        if 'band' in opt_option:
            DEFAULT_START_PARA = [3.456, 4.567]


            res = Bayesian_optimization(cov_radius, n_calls=100)
            print('Best parameter:\n', res.x,'\n', res.fun)
            r0_best, r0_dens_best = res.x

            os.chdir(p)
            plot_convergence(res)
            plt.savefig('convergence.png', dpi=300)
            plt.clf()

            eval = plot_evaluations(res, bins=20)
            eval[0][0].figure.savefig('eval.png', dpi=300, bbox_inches='tight')

            obje = plot_objective(res, sample_source='result')
            obje[0][0].figure.savefig('obj.png', dpi=300, bbox_inches='tight')
        else:
            # r0_best, r0_dens_best = hp.atoms_info[atomic_symbols[-1]]['r_wave'], hp.atoms_info[atomic_symbols[-1]]['r_dens']
            r0_best, r0_dens_best = 2 * cov_radius, 4 * cov_radius

            mu = Loss_EnergyGeometry([r0_best, r0_dens_best])
            print(f"mu: {mu}")


    # ============================================================================ #
    # Post processing

        # Save the best result

        os.chdir(p)
        best_folder = p / DEFAULT_OUT_DIR / f"opt_{r0_best:.3f}_{r0_dens_best:.3f}"
        backup_dir  = p / f"opt_{r0_best:.3f}_{r0_dens_best:.3f}"

        os.system(f"cp -r {best_folder} {backup_dir}")
        os.system("tar -zcvf results.tgz results && rm -r results")
    #    os.system("rm -r results")

    # ============================================================================ #
    # Plot convergent picture



if args_input.user_defined_parameters is not None:

    r0, r0_dens, c_rep, sigma = args_input.test_para

    dft_infile = p / args_input.input_dir / DEFAULT_DFT_FILE
    dft_input, dft_center, atomic_numbers, atomic_numbers_exclusive , atomic_symbols, atomic_symbols_exclusive, bolt_f, bolt_f11, cov_radius = get_input(dft_infile)

    confinement_r0, confinement_sigma = set_parameters([r0, r0_dens], 
                                                       sigma)
    
    hp.set_variables(confinement_r0,
                     confinement_sigma, 
                     xc=DEFAULT_XC,
                     superposition=DEFAULT_SUPERPOSITION,
                    )
                    
    opt_directory = p / DEFAULT_OUT_DIR / f"opt_{r0:.3f}_{r0_dens:.3f}"
    opt_directory.mkdir(exist_ok=True, parents = True)
    os.chdir(opt_directory)

    hp.full_hotcent(superposition=DEFAULT_SUPERPOSITION,
                    opt_elem=opt_elem,
    )

    dft_incopy = copy.deepcopy(dft_input)
    # dftb_bandstructure_final, dft_subincopy_final = run_calculator_seperately(dft_incopy, DEFAULT_KPOINTS_DENSITY)
    # dftb_bandstructure_final, dft_subincopy_final = ase.io.read('dftb_band.xyz', ':'), ase.io.read('dft_final.xyz', ':')
    dftb_bandstructure_final, dft_subincopy_final = run_calculator_seperately_parallel(dft_incopy, DEFAULT_KPOINTS_DENSITY, 'calc_dftb.py', 'static')


    mu = parallel_repulsion(np.array([c_rep]), r0, r0_dens, write_rep=False)
    print("mu:", mu)

    # if len(opt_elem) > 0:
    #     pr.write_specific_skf(opt_elem)
    # else:
    #     pr.write_skf()

    if opt_elem == None or len(opt_elem) == 0:
        dftb_full = ase.io.read('dftb_full.xyz', ':')

        with open ("search_index.txt", 'r') as search_index_file:
            search_index = search_index_file.read().strip().split(',')
        bolt_f11_final = [bolt_f11[int(i)] for i in search_index]

        dftb_energy_per_atom_relative, dft_energy_per_atom_relative, delta_energy_per_atom = get_energy_per_atom(dft_incopy, dftb_full, bolt_f11_final)
        hp.plot_vs(atomic_symbols[0], [r0, r0_dens, c_rep, mu], dft_incopy, dft_energy_per_atom_relative, dftb_energy_per_atom_relative, bolt_f)

        hp.plot_tradition(atomic_symbols[0], [r0, r0_dens, c_rep, 2.0],  mu, dft_energy_per_atom_relative, dftb_energy_per_atom_relative, bolt_f)



t1 =    perf_counter()  


print('The running time is {} seconds\n'.format(t1-t0))


