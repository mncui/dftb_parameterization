# %%

import copy
import os
from math import pi, sqrt
from pathlib import Path

import ase
import ase.io
import matplotlib.pyplot as plt
import numpy as np
from ase.data import atomic_numbers, chemical_symbols, covalent_radii
from ase.units import Bohr
from skopt import gp_minimize, load
from skopt.callbacks import CheckpointSaver
from skopt.plots import (plot_convergence, plot_evaluations,
                         plot_gaussian_process, plot_objective,
                         plot_objective_2D)
from skopt.space import Categorical, Integer, Real

from hotpy6 import hotpy


def monkhorstpack2kptdensity(atoms, k_density):

    """Convert Monkhorst-Pack grid to k-point density.
    atoms (ase.atoms.Atoms): Atoms object.
    k_grid (list): [nx, ny, nz].
    Returns:
        float: Smallest line-density.
    """

    # assert len(k_grid) == 3, "Size of k_grid is not 3."

    recipcell = atoms.cell.reciprocal()
    kd = [] # k-point density
    ks = [] # k-point space
    # initial a array for k_grids, with 3 columns
    # k_grids = np.empty((1,3))
    k_grid_abc = []
    for i in range(3):
        if atoms.pbc[i]:
            k_grida = int(k_density * (2 * pi * sqrt((recipcell[i] ** 2).sum())))
            if k_grida == 0:
                k_grida = 1
            k_grid_abc.append(k_grida) # current k_grid
            # k_grids = np.vstack((k_grids, k_grid_abc))
            # kptdensity = k_grid[i] / (2 * pi * sqrt((recipcell[i] ** 2).sum()))
            kptspace =  sqrt((recipcell[i] ** 2).sum()) / k_grida
            ks.append(kptspace)
    return ks, k_grid_abc

def band_search(superposition, para_list):
    """
    Grid search for the best parameters for band structures

    Parameters:
    superposition: str
        Superposition of the band structure, "density" or "potential"

    para_list: list
        List of parameters, eg. [r_wave, sigma]
    
    Returns:
        best_parameters file

    """

    # ============================================================================ #
    # 1. Generate skf

    global r_wave, r_dens
    r_wave, sigma = para_list
    r_dens = r_wave
    con1 = [r_wave, r_dens]

    P = Path('.')
    dft_folder = P
    dft_data = ase.io.read('dft.xyz', ':')
    json_file = 'dft.json'
    ini_hp = hotpy()

    # For multi-element system, we need to define the cons and sigs and c_rep for each element
    # First, define which element that you want to parameterize
    # opt_elem = [35]
    otot_elem_number = np.unique(np.array([np.unique(mole.numbers).tolist() for mole in dft_data]).flatten()) # get atomic_number
    elem_number = [i for i in otot_elem_number if i not in opt_elem] # remove O
    tot_elem_number = elem_number + opt_elem

    # Second, get rid of this element from element_numbers
    elem_symbol = [chemical_symbols[i] for i in tot_elem_number if i not in opt_elem]
    tot_elem_symbol = elem_symbol + [chemical_symbols[num] for num in opt_elem]  # Keep optimized element in the end
    # Third, set cons as a dictionary like cons = {'Si': [r_wave, r_dens], 'O': [r_wave, r_dens], 'H': [r_wave, r_dens]}, 
    # and sigs as a dictionary like sigs = {'Si': sig1, 'O': sig1, 'H': sig1}
    para_dict = {symbol: [ini_hp.atoms_info[symbol]['r0'], ini_hp.atoms_info[symbol]['r0']] for symbol in elem_symbol}
    # Fourth, add the element that I want to parameterize into the end of the dictionary manually
    para_dict[chemical_symbols[opt_elem[0]]] = con1
    # Finally, set sigs in the same way
    sigma_dict = {symbol: ini_hp.atoms_info[symbol]['sigma'] for symbol in elem_symbol}
    sigma_dict[chemical_symbols[opt_elem[0]]] = sigma

    # para_rep = [ini_hp.atoms_info[symbol]['c_rep'] for symbol in elem_symbol]


    # Print log infomation
    xc        = 'GGA_X_PBE+GGA_C_PBE'      # Exchange and Correlation
    print("paras_dict:", para_dict, "\nsigma_dict:", sigma_dict)
    print("xc:", xc, "\nsuperposition:", superposition)

    hp = hotpy(cons=para_dict,   # dict eg. {'Zn': [3.229, 3.229], 'O': [3.229, 3.229]}
              elements=tot_elem_symbol,     # list eg. ['Zn', 'O']
              sigma=sigma_dict,   # dict eg. {'Zn': 2.0, 'O': 2.0}
              workdir='./', 
              slator_p='./')

    print('Generating skf...')
    try:
        opt_chem = [chemical_symbols[i] for i in opt_elem]
        # hp.parallel_hotcent(xc, superposition, opt_chem) 
        hp.hotpot(xc, superposition)  

    # ============================================================================ #
    # 2. DFTB band calculation
        k_density = 9.0
        bolt_f, _ = hp.get_bolt_factor(dft_data, 'identical')
        dftdata = copy.deepcopy(dft_data)
        hp.calc_band_binary(para_list, dftdata, k_density, bolt_f)

    # ============================================================================ #
    # 3. Delta calculation
        dftdata = copy.deepcopy(dft_data)
        mu_band_homo, mu_band_lumo = hp.delta_band_binary(para_list,  json_file, dftdata, bolt_f) 

        # mu_band = delta_homos#np.sum(delta_band)
        print('\ntot_bolt:', bolt_f)
        print("effective_bolt_f:", [(bolt_f[i]) for i in range(len(bolt_f)) if bolt_f[i] >= 0.1], "\nmu_band_homo:", mu_band_homo, "\nmu_band_lumo:", mu_band_lumo)#, "\ndelta_homos:", delta_homos, "\ndelta_lumos:", delta_lumos)

        # ============================================================================ #
        tgz_file = '_'.join([str(round(i,3)) for i in para_list])
        os.system("tar -zcvf {}  *.skf && mv {} skf/ && rm *.skf".format( 'skf_' + tgz_file + '.tgz', 'skf_' + tgz_file + '.tgz'))
        return (mu_band_homo, mu_band_lumo)
    except(Exception) as err:
        print(err, "\nError in grid_search_band for band structure calculation!")
        return(10,10)


def loss(para_list):
    # print("\n#===========r_wave/sigma:Zn:{}/{}, O{}/{}=================#".format(para_list[0], para_list[1], para_list[2], para_list[3]))
    r0, s=para_list
    mu_band_homo, mu_band_lumo = band_search('potential', para_list)
    mu = mu_band_homo + mu_band_lumo

    # para_name = ',    '.join([str(round(i,7)) for i in para_list]) 
    # [band_delta.write("{:15.7f},".format(i)) for i in para_list]
    penalty = ((r0 / (5.796) - 1)**2 + 2)**2
    mu = mu * penalty
    
    band_delta.write(" {:15.7f}, {:15.7f}, {:15.7f}, {:15.7f}, {:15.7f}\n".format(para_list[0], para_list[1], mu_band_homo, mu_band_lumo, mu))#, delta_homos[0], delta_lumos[0], mu)) # str(r_wave) + '\t' + str(r_dens) + '\t' + str(mu_band) + '\n')
    band_delta.flush()
    return(mu)



# ============================================================================ #
# Initialize data
global opt_elem, cov_raddi

band_delta = open("band_delta.out", 'w')
band_delta.write("#r0,sigma,mu_homo,mu_lumo,mu_tot\n")
hp_ini = hotpy()
opt_elem = [8]
cov_radii = covalent_radii[opt_elem[0]] / Bohr

os.system('rm -r skf *.skf; mkdir skf')



# ============================================================================ #
# Set the search engine
# (1) Search space 
r0_lower = 0.6 # Bohr
r0_upper =  8 # Bohr
sigma_lower, sigma_upper = 1.2, 19.9

r0   = Real(name='r_wave',  low=r0_lower * cov_radii,  high=r0_upper * cov_radii,   prior='uniform')
sigma   = Real(name='r_dens',  low=sigma_lower,  high=sigma_upper,   prior='uniform')
dimensions = [r0, sigma]
default_parameter = [3.0, 2.0]

# (2) Search engine
read_pkl = 'no'
checkpoint_saver = CheckpointSaver("./checkpoint.pkl", compress=9, store_objective=False)  # lifesaver
if read_pkl.lower() == 'no':
    res = gp_minimize(func=loss,
                      dimensions=dimensions,
                      n_calls=300,
                      acq_func="EI",
                      x0=default_parameter,
                      callback=[checkpoint_saver],
                      random_state=1234)


elif read_pkl.lower() == 'yes':
    from skopt import load
    res  = load('checkpoint.pkl')
    x0   = res.x_iters
    y0   = res.func_vals

    res = gp_minimize(func=loss,
                      dimensions=dimensions,
                      n_calls=400,
                      acq_func="EI",
                      x0=x0,
                      y0=y0,
                      callback=[checkpoint_saver],
                      random_state=1234)

print('Best parameter:\n', res.x,'\n', res.fun)



# ============================================================================ #

plot_convergence(res)
plt.savefig('convergence.png', dpi=300)
plt.clf()

eval = plot_evaluations(res, bins=20)
eval[0][0].figure.savefig('eval.png', dpi=300, bbox_inches='tight')

obje = plot_objective(res, sample_source='result')
obje[0][0].figure.savefig('obj.png', dpi=300, bbox_inches='tight')

r_zn, s_zn, r_o, s_o = res.x
print("Zn:", r_zn, s_zn, "\nO:", r_o, s_o, "\ndelta_band:", res.fun)


os.system("mkdir dat; mv *.json *.png *.skf *.pkl dat/")

