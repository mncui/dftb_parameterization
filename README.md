# The Parameterization code for Density functional Tight-binding Theory (DFTB)
What does it includes?
#### 1. Quick SKF file generator

<div style="background-color:#F8D7DA; padding:10px">
<strong>Tip:</strong>
SKF(Slater-Koster file) includes integral tables describing the Hamitonian and overlap matrix elements on an equidistant grid, where Hyperparmeters for confinement potential were embeded with the way of confinement pseudo-atomic orbitals.
</div>

For detail format of SKF, reference to [SlaterKosterFiles][1].
- [x] How to quickly generate skf file based on exists parameters (eg. [QUASINANO][2], [PTBP][3])
- [x] How to quickly generate skf file with your own parameters

#### 2. Parameterization (atomwise, comparing to pairwise)
- [x] How to optimize parameters based on Energy & Geometry(the repetition of Paper [PTBP][3])
- [ ] How to optimize parameters based on Band structure
- [ ] What if I have a dataset, with energy & forces, how to optimize parameters

#### 3. Parameterization (pairwise)
- [ ] Stoichastic gradiend descent



## 1. Quick SKF file generator
The skf_generator based on [Hotcent][4] for bandstructure(H/S) part generation, and [Phyrep][5] for repulsive part generation.

[1]: https://dftb.org/fileadmin/DFTB/public/misc/slakoformat.pdf "Format of the v1.0 Slater-Koster files"
[2]: https://doi.org/10.1021/ct4004959 "DFTB Parameters for the Periodic Table: Part 1, Electronic Structure"
[3]: https://doi.org/xx "Obtaining Robust Density Functional Tight Binding Parameters for Solids Across the Periodic Table"
[4]: https://gitlab.com/mvdb/hotcent "Hotcent Gitlab page"
[5]: https://gitlab.com/jmargraf/physrep "Physrep Gitlab page"

### 1 One want to generate skf file based on existed parameter set

#### 1. QUASINANO
```
python skf_generator.py -s C H -p quasinano -hs -rep
```
- `-s`: for symbols
- `-p`: for the existed parameters set that you want to choice
- `-hs`: option for generating hamitonian and overlap matrix in skf file
- `-rep`: option for generation repulsive part 


#### 2. PTBP
```
python skf_generator.py -s C H -p ptbp -hs -rep
```
- `-s`: for symbols
- `-p`: for the existed parameters set that you want to choice
- `-hs`: option for generating hamitonian and overlap matrix in skf file
- `-rep`: option for generation repulsive part


### 2. One want to generate skf file based on their own parameters
```
python skf_generator.py -s C H -p personal -hs -r0 3.0 4.0 2.0 3.0 -sigma 2.0 2.0 -rep 0.5 1.1
```
- `-s`: for symbols
- `-p`: here we choice `personal`, the we need to provide the parameters as following
- `hs`: option for generating hamitonian and overlap matrix in skf file
- `r0`: the r0 value for confinement potential, we need two for each atom, 
in this example, we defined
```
-r0 3.0 4.0 (Atom C: the parameters wavefunction and density, respectively)
    2.0 3.0 (Atom H: the parameters wavefunction and density, respectively)
```
- `sigma`: the sigma value for confinement potential for `C` and `H` in this case
- `rep`: the parameters for repulsive part for `C` and `H`, respectively


## 2. Parameterization
#### 1. Elementary and Binary
##### Necessary preparations
- folder that contain reference xyz file from DFT
- set variable as `opt_elem=[]` in `DftbParameterization.py`
Note: the differences between the parameterization of elementary and binary are 1. varialbe `opt_elem` setting and 2. reference XYZ file

##### Command
```
python DftbParameterization.py -opt band rep -input dft
```
- `-opt`: to specific what would like to optimize
    - `-opt band rep`: the normal way to optimize $r^0_{w}$, $r^_{d}$ and $c_{rep}$ for confinement potential and repulsive potential simutaneousely.
    - `-opt rep`: only optimize $c_{rep}$ for repulsive potential, in this way one have to specify the default $r^0_{w}$, $r^_{d}$ in `DftbParameterization.py` code in the line of `r0_best, r0_dens_best = 2 * cov_radius, 4 * cov_radius`
    - `-opt band`: in this case, the loss function will be change to band structure automatically, be carefully to the reference folder, where one need to calculate band structure first for DFT
    
- `-input`: folder that contain reference DFT file

As examples, we provide dft/ folder, where dfts.xyz and nitrides.xyz inculde DFT energy/forces for elementary Cu and binary Nitrides as the references, one can try to run commands and repeat the results that we got in the article.
1. For Elementary Cu
- make sure that `opt_elem=[]` and `DEFAULT_DFT_FILE=dfts.xyz` in `DftbParameterization.py`, 
```
python DftbParameterization.py -opt band rep -input dft/ 
```
2. For Binary Nitrides
- make sure that `opt_elem=[7]` and `DEFAULT_DFT_FILE=nitrides.xyz` in `DftbParameterization.py`
```
python DftbParameterization.py -opt band rep -input dft/
```








